---
title: "Publishing Malayalam Speech Recognition Model"
tags: ["malayalam","speech","vosk", "kaldi","asr"]
date: 2021-10-19
author: "Kavya Manohar"
description: "Automatic Speech Recognition in Malayalam using kaldi and Vosk"
---

Malayalam speech recognition model trained on various openly available speech and text corpora using Kaldi toolkit is now released [here](https://gitlab.com/kavyamanohar/vosk-malayalam/-/tree/master/MODELS). It is now available for testing on the [Vosk-Browser Speech Recognition Demo website](https://kavyamanohar.github.io/vosk-browser/). This Malayalam model can be used with [Vosk](https://alphacephei.com/vosk/) speech recognition toolkit which has bindings for Java, Javascript, C# and Python.

A speech recognition architecture that works best in scenarios of limited speech data availability is called a pipeline model, where it is composed of an acoustic model, a language model and a phonetic lexicon.

{{< figure src="/img/awsar/asr.jpg">}}

Our acoustic model is trained on approximately 18 hours of Malayalam speech using [Kaldi](https://kaldi-asr.org/) toolkit. The N-gram language model is trained on [SMC Text Corpus](https://gitlab.com/smc/corpus) and the speech transcripts. The phonetic lexicon of more than [121k Malayalam words](https://gitlab.com/kavyamanohar/malayalam-phonetic-lexicon/-/blob/master/vocabASR) was created using [Mlphon](https://pypi.org/project/mlphon/) toolkit. For more details on training recipe check the [repository](https://gitlab.com/kavyamanohar/vosk-malayalam).


*Demo video*

<iframe width="560" height="315" src="https://www.youtube.com/embed/6TfOaVMgvOc?si=8drwhjUrbbNnoclw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>