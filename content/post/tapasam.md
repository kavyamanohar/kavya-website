---
title: "താപസം സെമിനാർ 2024"
tags: ["seminar", "malayalam"]
date: 2024-10-03
---

[താരതമ്യപഠനസംഘം](https://www.tapasam.com/) ഒക്ടോബർ 1, 2 തീയതികളിലായി സംഘടിപ്പിച്ച താപസം സെമിനാർ ശ്രീശങ്കരാചാര്യ സംസ്കൃതസർവ്വകലാശാലയിൽ വെച്ച് നടന്നു. ഈ സെമിനാറിൽ 'യൂണിക്കോഡിലെത്തിയ മലയാളം: ചില ഭാഷാസാംസ്കാരികചിചാരങ്ങൾ' എന്ന വിഷത്തിൽ ഞാനവതരിപ്പിച്ച പ്രഭാഷണം ഇവിടെ കൊടുക്കുന്നു.


<iframe width="560" height="315" src="https://www.youtube.com/embed/K_PGz2xu32c?si=atiFq4BbO7TuhkkT" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>