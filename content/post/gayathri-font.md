---
title: "Gayathri: New Malayalam Typeface"
tags: ["malayalam","unicode","script", "font", "typeface", "opentype", "gayathri" ]
date: 2019-02-23
description: "Gayathri Malaylam Font Release"
---

{{< figure src="/img/gayathri-font/gayathri.jpg">}}

[Swathanthra Malayalam Computing](https://smc.org.in/fonts) proudly announces Gayathri – a new typeface for Malayalam. Gayathri has its glyphs designed by [Binoy Dominic](http://binoydominic.com/), opentype engineering done by [Kavya Manohar](https://kavyamanohar.com/) and the project coordinated by [Santhosh Thottingal](https://thottingal.in/).

This typeface was financially supported by [Kerala Bhasha Institute](http://www.keralabhashainstitute.org/), a Kerala government agency under department of cultural affairs.

{{< figure src="/img/gayathri-font/release.jpg">}}

Gayathri is a display typeface, available in Regular, Bold, Thin style variants. It is licensed under Open Font License. Source code, including the SVG drawings of each glyph is available in the repository. Gayathri is available for download from [smc.org.in/fonts#gayathri](https://smc.org.in/fonts#gayathri)

Gayathri has soft, rounded terminals, strokes with varying thickness and good horizontal packing. Gayathri has large glyph set for supporting Malayalam traditional orthography, which is the new trend in contemporary Malayalam. With a total of 1124 glyphs, Gayathri also has basic latin coverage. All Malayalam characters defined till Unicode 11 is supported.

{{< figure src="/img/gayathri-font/ml-unicode.png">}}


This is the first typeface by Binoy Dominic. He had proved his lettering skills in his profession as graphic designer, working on branding with Malayalam content for his clients.

Being a typeface with traditional glyph set, there were scopes for huge number of stacked conjuncts. Gayathri is primarily designed for titles and large displays. Avoiding too much of below base stacking was hence a design plan. The conjunct glyph set was selectively decided, it kept changing as the design proceeded. Carefully tabulated opentype rule set ensured ു, ൂ signs never gets separated from their consonants in valid usecases. Also longer conjuncts would be split up in logically valid ways using [conditional clustering opentype rules](https://blog.smc.org.in/conditional-stacking/).

{{< figure src="/img/gayathri-font/conditionalclustering.png">}}

Binoy prepared all glyphs in SVGs, automated scipts converted it to [UFO sources](http://unifiedfontobject.org/). [Trufont](https://github.com/trufont/trufont) was used for minor edits. Important glyph information like bearings, names, were defined in yaml configuration. Build scripts generated valid UFO sources and [fontmake](https://github.com/googlei18n/fontmake/) was used to build OTF output. Gitlab CI was used for running the build chain and testing. Fontbakery was used for quality assurance. [UFO Normalizer](https://github.com/unified-font-object/ufoNormalizer), [UFO Lint](https://github.com/source-foundry/ufolint) tools were also part of build system. This automation done by [Santhosh Thottingal](https://thottingal.in/) has simplified the build process. Gayathri stands out in technical perfection of build chain compared to all other fonts by Swathanthra Malayalam Computing.

The project took almost an year to reach completion. There were multiple rounds of designing, redesigning, optimizing steps until it was released on Feb 21, 2019. The graph below indicates the [contribution history, retrieved from gitlab](https://gitlab.com/smc/fonts/gayathri/graphs/master).

{{< figure src="/img/gayathri-font/contribution.png">}}