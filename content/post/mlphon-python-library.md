---
title: "Malayalam Phonetic Analyser: Version 1.0.0"
tags: ["malayalam","unicode","script", "grapheme", "phoneme", "g2p" , "fst", "pypi", "python" ]
date: 2018-12-26
description: "Mlphon: A  multifunctional grapheme to phoneme conversion tool Malayalam using Finite state transducers"
---

Edit (September 20,2022): A detailed report on this is now available as a [journal article](https://kavyamanohar.com/post/mlphon-paper/)

In the [previous post](https://kavyamanohar.com/post/malayalam-phonetic-analyser/), I had shared the work in progress version of a finite state transducer based Malaylam phonetic analyser. A phonetic analyser analyses the written form of the text to give the phonetic characteristics of the grapheme sequence.

Understanding the phonetic characteristics of a word is helpful in many computational linguistic problems. For instance, translating a word into its phonetic representation is needed in the synthesis of a [text to speech (TTS)](https://en.wikipedia.org/wiki/Speech_synthesis) system. The phonetic representation is helpful to transliterate the word to a different script. It will be useful if the phonetic representation can be converted back to the grapheme sequence.

The first version of project **mlphon** is now released. It is packaged as a python library in [Pypi](https://pypi.org/project/mlphon/). You can now install it by 

    pip install mlphon


It has built-in methods for bidirectional grapheme to phoneme conversions, IPA mappings and a syllablizer. These three functions has command line tools as well. Tryout for yourself.

### Examples

#### Syllablizer

    $ mlsyllablizer

For the input 

    സഫലമീയാത്ര

the output would be

    <BoS>സ<EoS><BoS>ഫ<EoS><BoS>ല<EoS><BoS>മീ<EoS><BoS>യാ<EoS><BoS>ത്ര<EoS>

    ['സ', 'ഫ', 'ല', 'മീ', 'യാ', 'ത്ര']

`<BoS>` indicate the beginning of a syllable and `<EoS>` the end of a syllable.
#### G2P analysis and synthesis

     $ mlg2p -a

Give the input 

    കാവ്യ

It will give you the result of g2p analysis as:

    <BoS>k<plosive><voiceless><unaspirated><velar>aː<v_sign><EoS><BoS>ʋ<approximant><labiodental><virama>j<glide><palatal>a<schwa><EoS>

The details of each phoneme are given in angle brackets. The operation is bidirectional. You can retrieve the graphemes from the analysis string.

#### IPA analysis and synthesis

If the phonetic detailing is not relevant to you, a minimal mapping of the graphemes to IPA can be obtained by 

    $ mlipa -a

For the input

    കൽക്കണ്ടം

The output would be 
    
    kal<chil>kkaɳʈam<anuswara>

Certain tags like `<chil>, <anuswara>, <visaraga>` are retained so that bidirectional analysis and generation are unambiguously possible.

More details on its usage is available in the [PyPi documentation](https://pypi.org/project/mlphon/) as well as in the README section of [mlphon](https://gitlab.com/smc/mlphon/) repository.

Will update the progress here. For a quick web demo of what mlphon does, checkout this link https://phon.smc.org.in/ 

 Thanks for reading 😀.

### References

1. [Malyalam Phonetic Analyser git repository](https://gitlab.com/smc/mlphon/)
1. [Pypi repository of mlphon](https://pypi.org/project/mlphon/)
1.  [Malyalam morphological analyser using finite state transducers](https://github.com/smc/mlmorph)
1.  [The Festvox Indic Frontend for Grapheme-to-Phoneme Conversion](https://www.parlikar.com/files/aup_wildre_2016.pdf)
1.  [Malayalam Phonetic Archive by Thunchath Ezhuthachan Malayalam University](http://www.cmltemu.in/phonetic/#/)
1.  [IPA and sounds](http://www.internationalphoneticalphabet.org/ipa-sounds/ipa-chart-with-sounds/)
