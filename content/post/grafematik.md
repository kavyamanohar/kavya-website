---
title: "Talk on 'Malayalam orthographic reforms' at Grafematik 2018"
tags: ["malayalam","unicode","script", "script-reformation", "oldlipi", "newlipi" , "font", "conference", "grafematik" ]
date: 2018-06-21
description: "Grafematic Conference, Script Reformation in Malayalam"
---

{{< figure src="/img/grafematik/imt.jpg">}}

Santhosh and I presented a paper on 'Malayalam orthographic reforms: impact on language and popular culture' at Graphematik conference held at [IMT Atlantique](http://www.imt-atlantique.fr/fr), Brest, France on 14th and 15th of June, 2018. Our session was chaired by [Dr. Christa Dürscheid.](https://en.wikipedia.org/wiki/Christa_D%C3%BCrscheid)

 The paper we presented is available [here](https://thottingal.in/documents/Malayalam%20Orthographic%20Reforms_%20Impact%20on%20Language%20and%20Popular%20Culture.pdf). The video of our presentation is available in [youtube](https://www.youtube.com/watch?v=-KvFuuVix4Q).

 <iframe src="https://www.youtube.com/embed/-KvFuuVix4Q" allowfullscreen="allowfullscreen" width="560" height="315" frameborder="0"></iframe>

 [Grafematik](http://conferences.telecom-bretagne.eu/grafematik/) is a conference, first of its kind, bringing together disciplines concerned with writing systems and their representation in written communication. There were lot of interesting talks on various scripts around the world, their digital representation, role of Unicode, typeface design and so on. All the talk videos are available in the [conference website](http://conferences.telecom-bretagne.eu/grafematik/).

<sub><sup>This post is originally published [here](https://thottingal.in/blog/2018/06/21/grafematik-2018/).</sup></sub>
