---
title: "Malayalam: Life and Praxis - Seminar Series at Tirur"
tags: ["seminar", "malayalam"]
date: 2025-02-20
---
A three day National Seminar, "Malayalam: Life and Praxis", was organized by the Tirur Regional Centre of [Sree Sankaracharya University of Sanskrit](https://ssus.ac.in/) during February 18-20, 2025 as a tribute to Dr. Sushama L., Professor on Malayalam Lingustics, who is retiring from her teaching career in this academic year.  Dr. Sushama currently serves as the Vice Chancellor of [Thunchath Ezhuthachan Malayalam University](http://malayalamuniversity.edu.in/ml/).

{{< figure src="/img/tirur-ai-ml/kavya.jpeg">}}


I was invited to deliver a session on "[കമ്പ്യൂട്ടർ മനസ്സിലാക്കുന്ന മലയാളഭാഷ](https://kavyamanohar.github.io/tirur-ml-ai-talk/)". My talk was intended to address a non-technical audience to the world of Computational Linguistics and AI language models. It was incredible to share this stage with Santhosh Thottingal, whose presentation focused on the evolution of the form and function of digital Malayalam in the past 20 years. 

{{< figure src="/img/tirur-ai-ml/santhosh.jpeg">}}

Enjoyed the panel discussion that followed the talk. The event was well coordinated under the initiative of Dr. Shaji Jacob. Happy to have met many eminent professors on Malayalam and Linguistics.


It has been really long since I travelled for a talk-session. Feels good about visiting the famous [Mamangam](https://en.wikipedia.org/wiki/Mamankam) monuments - നിലപാടുതറ, മണിക്കിണർ, മരുന്നറ, നാവാമുകുന്ദക്ഷേത്രം and താമരക്കായൽ - around Tirunavaya

{{< figure src="/img/tirur-ai-ml/thamarakkayal.jpg"caption="താമരക്കായൽ, തിരുനാവായ">}}
