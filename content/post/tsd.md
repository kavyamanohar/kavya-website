---
title: "Text Speech and Dialogue: TSD 2020"
tags: ["malayalam","speech","morphology", "linguistics","conference", "tsd" ]
date: 2020-09-12
---

I presented a paper on Quantitative Analysis of the Morphological Complexity of Malayalam Language at [23rd International Conference on Text, Speech and Dialogue: TSD 2020](https://www.tsdconference.org/tsd2020/index.html), Brno, Czech Republic, September 8–11 2020. The year being 2020, the entire conference happened in remote participation mode.

Conference proceedings and pre-recorded presentation videos were made available to the participants and we discussed it over online zoom sessions. It was a novel experience and I am super excited about how I got feedbacks and ideas to work on, even after the live sessions.

*Conference proceedings*: [Springer LNCS](https://link.springer.com/book/10.1007/978-3-030-58323-1)

*My paper*: [Quantitative Analysis of the Morphological Complexity of Malayalam Language](https://kavyamanohar.com/documents/tsd_morph_complexity_ml.pdf)

*Detailed discussion of the paper*: [Blogpost](https://kavyamanohar.com/post/malayalam-morphological-complexity/)

*Data and Code*: [Gitlab Repo](https://gitlab.com/kavyamanohar/malayalam-ttr)

*Presentation Slides*:

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQy5oQvmQHF1hbWS9qgcHUbXwOH5MZb5wydpcs3awHviZ71ZOPrV-q3Rm21qjbjMkG-VrPpaYEEEISP/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

*Presentation video*

 <iframe src="https://www.youtube.com/embed/yFpA6HcZHUU" allowfullscreen="allowfullscreen" width="560" height="315" frameborder="0"></iframe>

Thanks for reading 😀