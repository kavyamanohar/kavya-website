---
title: "An Open Framework to Build Malayalam Speech to Text System"
tags: ["malayalam", "science congress", "Kerala" ]
date: 2023-02-28
description: "Grafematic Conference, Script Reformation in Malayalam"
---

{{< figure src="/img/ksc-2023/award.png">}}

It was indeed a pleasure to present my paper on *An openframework to develop Malayalam Speech to text Systems* at the 35th Kerala Science congress held during 10th-14th of February, 2023 at Kuttikkanam, Kerala India. The work was presented in the category of Scientific Social Responsibility and recieved the best oral presentation award in that category.

The presentation was all about how I ensured openness and transperancy in the development process of speech recognition system for Malayalam done as part of my 
PhD work **Linguistic Challenges in Malayalam Speech Recognition: Analysis and Solutions**.


 The paper  presented is available [here](https://docs.google.com/presentation/d/14m_KcGgRypFDsinPVJP_AxDk_7Z571VrDKdDDX_6RWc/edit?usp=sharing).

{{< figure src="/img/ksc-2023/kavya-ksc.png">}}
