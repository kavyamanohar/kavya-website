---
title: "Talks on Speech Recognition Research and Malayalam Computing"
tags: ["Speech Recognition", "Malayalam", "Interview"]
date: 2020-07-05
# draft: True
---

Sharing the videos of two informal interviews I did during the past few months. 

In this video I talk with Hrishikesh Bhaskaran on my involvement with SMC and my projects. This was the part of an interview series hosted by Tinker Hub Foundation.

<iframe width="560" height="315" src="https://www.youtube.com/embed/e7T-qoPOUOM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


In the following video I talk on Speech recognition systems in general, and on the [voice corpus](https://msc.smc.org.in/) initiative by [SMC](https://smc.org.in/). This interview is hosted by Mujeeb for [IB Computing](https://www.youtube.com/channel/UCet_TVCa8y0Eh31Eo78-hmg) Youtube channel.

 <iframe src="https://www.youtube.com/embed/bMHUFKQqJ4o" allowfullscreen="allowfullscreen" width="560" height="315" frameborder="0"></iframe>
