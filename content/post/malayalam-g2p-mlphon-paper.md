---
title: "Mlphon: A Multifunctional Grapheme-Phoneme Conversion Tool Using Finite State Transducers"
tags: ["malayalam","speech","g2p", "p2g","publication"]
date: 2022-09-20
author: "Kavya Manohar"
description: "Malayalam Language g2p | Grapheme to Phoneme in Malayalam"
---

The [Mlphon](phon.smc.org.in) tool I had been working on, for the past couple of years was extensively expanded as part of my research work at College of Engineering Trivandrum. A detailed presentation of the phonemic features of Malayalam, their incorporation as a sequential ruleset in the form of finite state transucers, a quantitative evaluation of the applications of the tool are now available in the [article, published by the open access journal, IEEE Access](https://ieeexplore.ieee.org/document/9877808).


{{< figure src="/img/mlphon-paper/g2p.png">}}


[Code Repository](https://gitlab.com/smc/mlphon)

[Download Pdf](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=9877808)

## Applications Using Mlphon

1. [Mozhi Malayalam-English code switched TTS](https://mozhi.me/)
2. [Malayalam Automatic Speech Recognition](https://ccoreilly.github.io/vosk-browser/)
3. [Web Demo of Malayalam Phonetic Analysis](https://phon.smc.org.in/)
