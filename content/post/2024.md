 ---
title: "Recap 2024"
tags: ["personal" ]
date: 2025-01-01
description: "A personal review of 2024"
---

> I write this not for the world, but for me.

In the first quarter of 2024, I struggled a bit to find an answer to whether I should switch back to the full time faculty position or just continue the current semi-academic reserch position and finally decided to stick with the latter. As the year ends, I gladly realize that the decision saved me from the mundaneness of many academic/administrative chores. 

The freedom of academic exploration at my current job led to my most prestigious publication of the year at EMNLP 2024. 

I now represent Digital University Kerala at the LITD-20 (Indian Language Technologies And Products) committee of Bureau of Indian Standards.

I am glad to have initiated some meaningful collaborations with researchers across the globe exploring various aspects of fair and inclusive NLP research and evaluation methods.

I realize the importance of nuanced linguistic expertise and I was able to give consultation to people in academia and industry on matters related to Indic language technologies, tools and their usage.

I gave an invited talk at Sree Sankaracharya Univerisity of Sanskrit, Kalady. 

An article I wrote on the the impact of AI on Language Technologies is now part of the textbook for second semester UG students in Calicut University.

I had the opportunity to mentor few passionate researchers leading to major conference publications.

I am glad again that I had very supportive collegues at work, which made life simple and smooth. The love and kindness I experience from my closest people helped me navigate the most difficult days, difficult not due to any real circumstance, but the perception of my own mind.

In 2024, I never tried to push the limits. Always took it a bit slow. It had its pros and cons.

Looking back, there are a lot of unfinished tasks. I want to have a pace up in 2025, personally and professionally. Will update!!


