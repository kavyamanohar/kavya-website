---
title: "Phonetic description of Malayalam consonants"
tags: ["Malayalam", "Phonetics", "Speech", "Grapheme", "Phoneme"]
date: 2020-01-18
---

The orthography (system for writing a language) of Malayalam is considered [phonemic](https://en.wikipedia.org/wiki/Phonemic_orthography) in nature. It means the graphemes (written symbols) correspond to the phonemes (significant spoken sounds) of the language. But the correspondence between graphemes and phonemes is not precisely one-to-one. The pronunciation of graphemes can depend on its position in a word (word beginning, middle or end) and its proximity to other graphemes.

It was two years back, I started to work on a [grapheme to phoneme conversion tool](https://kavyamanohar.com/post/malayalam-phonetic-analyser/) for Malayalam. It [transcribes](https://phon.smc.org.in/) any word in Malayalam script to the IPA ([International Phonetic Alphabet](https://en.wikipedia.org/wiki/International_Phonetic_Alphabet)) sequence of its pronunciation. Along with the IPA representation, it gives the manner and position of articulation of each phoneme. The Malayalam [phonetic archive project](http://www.cmltemu.in/phonetic/#/) of Thunchath Ezhuthachan Malayalam University was my initial reference. 

{{< figure src="/img/malayalam-phonetic-analyser/sample.png"caption="Phonetic Analysis of കേരളം using Malayalam Phonetic Analyser">}}


You can try out this tool here: https://phon.smc.org.in/

Later on when I started learning more on [articulatory phonetics](https://en.wikipedia.org/wiki/Articulatory_phonetics), I became bit skeptic on the rules I relied on to  transcribe Malayalam text to IPA. Upon referring different sources, I found there are slight disagreements among various grammarians and phoneticians. So I thought explore a bit and collect and record the details. Currently in this post the focus is on consonants. Vowels will be taken up later.

This post is going to be a document on the phonetic description of Malayalam consonants. I referred the following materials to prepare this document.

1. [കേരളപാണിനീയം](https://ml.wikisource.org/wiki/%E0%B4%95%E0%B5%87%E0%B4%B0%E0%B4%B3%E0%B4%AA%E0%B4%BE%E0%B4%A3%E0%B4%BF%E0%B4%A8%E0%B5%80%E0%B4%AF%E0%B4%82) - ഏ. ആര്‍. രാജരാജവര്‍മ്മ (Keralapanineeyam - A. R. Rajaraja Varma)
2. [The Theory of Lexical Phonology](https://link.springer.com/book/10.1007/978-94-009-3719-2) - K. P. Mohanan
3. [Malayalam (Descriptive Grammars)](https://books.google.co.in/books/about/Malayalam.html?id=uFQG2DCaIsIC) - R. E. Asher, T. C. Kumari
4. [Malayalam Phonetic Archive](http://www.cmltemu.in/phonetic/#/)-  Thunchath Ezhuthachan Malayalam University
5. [Malayalam IPA Help](https://en.wikipedia.org/wiki/Help:IPA/Malayalam) page on English Wikipedia 
6. [An Auditory and Acoustic Study of Liquids in Malayalam](https://core.ac.uk/download/pdf/40022785.pdf) - Dr. Reenu Punnoose
7. [IPA Chart with sounds](https://www.internationalphoneticalphabet.org/ipa-sounds/ipa-chart-with-sounds/)
8. [Malayalam (Namboodiri Dialect)](https://www.cambridge.org/core/journals/journal-of-the-international-phonetic-association/article/malayalam-namboodiri-dialect/19CBF6E9E1CE65A84928F7C9C2286A9B#fndtn-information) - Savithry Namboodiripad, Marc Garellek


## The Malayalam consonants

 The consonants in Malayalam (like all Brahmi script alphabets) are ordered as per the place and manner of articulation. While articulating vowels, the air from lungs flow unrestrictedly above the larynx through the vocal tract. But consonants are articulated by restricting airflow completely or partially by articulators like velum, tongue, teeth, lips etc. See [articulatory phonetics](https://en.wikipedia.org/wiki/Articulatory_phonetics) for detailed read on the role of articulators in human speech production mechanism. The following section describes the concept of manner and position of articulation. It is adapted from the book ['The Anthropology of Language']((https://www.amazon.com/Anthropology-Language-Introduction-Linguistic/dp/1337571008)) by Harriet Joseph Ottenheimer and Judith M.S. Pine. Apart from the manner and position, features like voicing and aspiration of a phoneme is also discussed.

#### Place of Articulation

It describes where the air flow is getting obstructed or modified.

1. **Glottal** - In the glottis (space between the vocal chords)
1. **Velar** - With the back of tongue and velum.
2. **Palatal** - With middle of tongue and hard palate (roof of mouth)
3. **Retroflex** - With tip of tongue and hard palate
4. **Alveolar** - With tip of tongue and alveolar ridge  (the ridge just behind the teeth)
5. **Dental** - With tip of tongue against upper teeth
6. **Labiodental** - With lower lip and upper teeth
6. **Bilabial** - With both lips

Articulatory phonetics describes other positions of articulation as well, I have omitted them because they are not relevant for Malayalam.

#### Manner of Articulation

It describes how the air flow gets modified at the articulatory position.

1. **Plosives** - Airflow is stopped at some position, and  then released out of the mouth
2. **Nasals** - Velum is lowered, air resonates and escapes through nasal cavity
3. **Fricatives** - Vocal tract is constricted to create a turbulent airflow.(Air hisses or buzzes)
4. **Affricate** - Fricative following a stop
5. **Tap** - A very brief stop formed by a single touch
6. **Trill** - A very brief stop, by many fast touches
7. **Approximant** - Air flows through constriction, but free to flow than in fricatives
8. **Lateral Approximant** - Airstream proceeds along the sides of the tongue, but it is blocked by the tongue from going through the middle of the mouth

#### Voicing

If the vocal chords vibrate during the articulation of a phoneme, it is said to be voiced. All vowels are voiced. Consonants are sometimes voiced and sometimes not. 

#### Aspiration

Pronunciation of a consonant followed by strong burst of air, makes it aspirated.

## Classifying consonants: The Keralapanineeyam way

The consonant alphabets in Malayalam, along with the manner and position of articulation as described by **Keralapanineeyam** is tabulated in Table 1. 

Manners of articulation are listed in the left column and positions of articulation is indicated in the top row.

|Position →→    Manner↓                      |  കണ്ഠ്യം (Velar) | താലവ്യം (Palatal) | മൂർദ്ധന്യം (Retroflex) | വർത്സ്യം (Alveolar) | ദന്ത്യം (Dental) | ഓഷ്ഠ്യം (Labial)|ഘോഷി (Glottal)|
|---                   | ------          | -------          |-------             |-------            |----            | ---    |---|
|ഖരം (Plosive, UV, UA) | ക              | ച                | ട                  | ഺ                 |ത             |പ       ||
|അതിഖരം (Plosive, UV, A)| ഖ            | ഛ               | ഠ                  |                   |ഥ            |ഫ      ||
|മൃദു (Plosive, V, UA)| ഗ                 | ജ                | ഡ                 |                   |ദ            |ബ       ||
|ഘോഷം (Plosive, V, A)| ഘ              | ഝ               | ഢ                 |                   |ധ            |ഭ       ||
|അനുനാസികം (Nasal)   | ങ              | ഞ               | ണ                 | ഩ                |ന            |മ       ||
|മദ്ധ്യമം (Approx./Lateral Approx.Tap/Trill)|                 | യ               |ര                    |                |ല             |വ      ||
|ഊഷ്മാക്കള്‍ (Fricatives)|                 |ശ                 |ഷ                 |                   |സ             |       |ഹ|
|ദ്രാവിഡമദ്ധ്യമം (Approx./Lateral Approx.Tap/Trill|       |                  |റ ഴ ള*               | ള                  |             |       ||

**Table 1**: The consonant graphemes in Malayalam, along with the manner and position of articulation as per the description in Keralapanineeyam. Please note the use of following abbreviations. V: Voiced, UV: Unvoiced, A: Aspirated, UA: Unaspirated, Approx.: Approximant

It is important to note the following aspects on the consonant graphemes in Malayalam and their articulatory features.

 - The nomenclature in the table for the manner and position of articulation follows the description in Keralapaneeyam. The most suitable translations to English are indicated in parenthesis.
 - The bilabial fricative sound as in **ഫാ**ന്‍ is commonly used in loan words and is written using the grapheme 'ഫ', which in fact is a labial aspirated plosive in native words.
 - The graphemes for alveolar plosive (ഺ)and alveolar nasal (ഩ) are not seen in regular Malayalam text usage. But those sounds are popular. 
    - The alveolar plosive sound occur in Malayalam  either in geminated form (eg: കാറ്റ്) or as a conjunct with the alveolar nasal(eg: എന്റെ). These use cases are addressed in text by the combination of letters in conjunct form as റ്റ (റ+ ് + റ) and ന്റ  (ന + ് + റ) respectively.
    - The alveolar nasal sound (as in പന) is represented by the letter ന, which in fact is the grapheme for dental nasal. So the phonemic manifestation of ന, varies contextually in the language.
- യ, ര, ല, വ are classified as മദ്ധ്യമം. But the manner of articulations of these actually ranges between those of approximants, laterals, taps or trills.
- ശ, ഷ, സ, ഹ are fricatives with varying positions of articulations as indicated in the table.
- The set ള, ഴ, റ is described as ദ്രാവിഡമദ്ധ്യമം. These are those sounds used only in Dravidian languages like Tamil, Malayalam etc.
- The description of ള in Keralapaanineeyam
    - The letter 'ള' is described as the retroflex (മൂർദ്ധന്യം) consonant in the following Sloka from a chapter titled 'അക്ഷരമാല':

        >   അ കവർഗ്ഗം കണ്ഠജമാം</br>
        >    ഇ ചവർഗ്ഗ യശങ്ങൾ താലവ്യം</br>
        >    ഉ പവർഗ്ഗ വ ഓഷ്ഠജമാം</br>
        >    ഋ ടവർഗ്ഗ രഷങ്ങൾ മൂർദ്ധന്യം</br>
        >    തവർഗ്ഗ ലസം ദന്ത്യം സന്ധ്യക്ഷരമൊത്തപോൽ ദ്വയസ്ഥാനം</br>
        >    **ര ഷ ള ഴ മൂർദ്ധന്യംതാൻ**</br>
        >    ഺ ഩ വർത്സ്യം ദ്രാവിഡം ഖിലീഭൂതം

    -  But in the same chapter ള is described as an alveolar in paragraph numbered 35 as:
        
        >ലകാരം ദന്ത്യമാണ്; അതിനെ ഉച്ചരിക്കുമ്പോൾ ജിഹ്വാഗ്രം ഉയർത്തി ശ്വാസവായുവിനെ ത്തടഞ്ഞ്, പിന്നീട് ജിഹ്വാഗ്രത്തിന്റെതന്നെ സ്പന്ദനം (തെറിപ്പിക്കൽ) കൊണ്ട് ജിഹ്വയുടെ ഇരുപുറത്തുംകൂടി വായുവിനെ ഇടവിട്ടിടവിട്ടു പുറത്തേക്കു വിടുകയാണ് കരണവിഭ്രമം. ഈ കരണവിഭ്രമംതന്നെ ദന്തമൂലത്തിൽ ചെയ്യുന്നതിനുപകരം ‘വർത്സം' എന്നു പറയുന്ന, വായുടെ മേൽത്തട്ടിന്റെ ഭാഗത്തിലാക്കിയാൽ ളകാരമായി. അതുകൊണ്ട് **വർത്സ്യമായ ലകാരംതന്നെ ളകാരം**.
        
        It defines both ല and ള as lateral approximants with ല being dental but ള alveolar. Thus the same grapheme ള is described as dental as well as alveolar within a span of few pages.


Assuming 'ള' is alveolar and classifying the non-plosives more precisely in terms of the manner of articulation, we have the following table (Table 2) consistent with the explanations in Keralapanineeyam. The classification in terms of exclusive Dravidian family (ദ്രാവിഡമദ്ധ്യമം) is omitted here.

| Position →→    Manner↓                      |  കണ്ഠ്യം (Velar) | താലവ്യം (Palatal) | മൂർദ്ധന്യം (Retroflex) | വർത്സ്യം (Alveolar) | ദന്ത്യം (Dental) | ഓഷ്ഠ്യം (Labial)|ഘോഷി (Glottal)|
|---                   | ------          | -------          |-------             |-------            |----            | ---    |---|
|ഖരം (Plosive, UV, UA) | ക               | ച                | ട                  | ഺ                 |ത             |പ       ||
|അതിഖരം (Plosive, UV, A)| ഖ            | ഛ               | ഠ                  |                   |ഥ            |ഫ      ||
|മൃദു (Plosive, V, UA)| ഗ                 | ജ                | ഡ                 |                   |ദ            |ബ       ||
|ഘോഷം (Plosive, V, A)| ഘ              | ഝ               | ഢ                 |                   |ധ            |ഭ       ||
|അനുനാസികം (Nasal)   | ങ              | ഞ               | ണ                 | ഩ                |ന            |മ       ||
|മദ്ധ്യമം (Approximant)|                   | യ               | ഴ                    |                   |               |വ      ||
|മദ്ധ്യമം (Lateral Approximant)|           |                  |                    |ള                  |ല               |     ||
|മദ്ധ്യമം (Tap)|           |               |ര                 |                   |                |     ||
|മദ്ധ്യമം-സ്ഫുരിതം (Trill)|           |               |റ                 |                   |                |     ||
|ഊഷ്മാക്കള്‍ (Fricatives)|                 |ശ                 |ഷ                 |                   |സ             |       |ഹ|

**Table 2**: The consonat graphemes in Malayalam. Manner of Articulation is made bit more precise than in Table 1. It conforms to the positions of articulation described in Keralapaanineeyam, 1916 (does not adhere to the modern phonological studies on Malayalam)

-  യ ഴ and വ are approximants in terms of the manner of articulation. യ is palatal, while വ is labiodental and ഴ retroflex. Apart from that യ is a [glide or semi-vowel](https://en.wikipedia.org/wiki/Semivowel)
- ള and ല are approximants, but with lateral (airstream proceeds along the sides of the tongue) properties. According to Keralapanineeyam ള is alveolar and ല is dental.
- ര is a retroflex tap (single touch of tongue)
- റ is a retroflex trill (vibrating touch of tongue)
- ശ, ഷ, സ, ഹ are fricatives with the following position of articulation
    - ശ is palatal
    - ഷ is retroflex
    - സ is dental
    - ഹ is glottal 

## Malayalam consonats and their IPA representation

[International Phonetic Alphabets(IPA)](https://en.wikipedia.org/wiki/International_Phonetic_Alphabet) are standardized notations for sounds in spoken language. So if you want to describe how a word 'sounds', then describe it in terms of IPA. How can you describe the consonants in terms of IPA symbols? There has been disgreements among different authors. Starting from the [phonetic archive project](http://www.cmltemu.in/phonetic/#/) of Thunchath Ezhuthachan Malayalam University(TEMU), let us see how it has been addressed in literature to reach a reasonable conclusion.

As per the phonetic archive project of TEMU, the consonants graphemes in Malayalam and their IPA representation are as in Table 3.

| Position →→    Manner↓      |  കണ്ഠ്യം (Velar)   | താലവ്യം (Palatal) | മൂർദ്ധന്യം (Retroflex) | വർത്സ്യം (Alveolar) | ദന്ത്യം (Dental) | Labiodental|ഓഷ്ഠ്യം (Labial)|ഘോഷി (Glottal)|
|---                   | ------            | -------          |-------             |-------            |----           |----             | ---    |---|
|ഖരം (Plosive, UV, UA) | ക  k             | ച   c             | ട   ʈ               | ഺ ṟ                |ത t         |             |പ p      ||
|അതിഖരം (Plosive, UV, A)| ഖ  kʰ          | ഛ    cʰ           | ഠ  ʈʰ                |                   |ഥ  tʰ       |           |ഫ  pʰ    ||
|മൃദു (Plosive, V, UA)| ഗ   g              | ജ ɟ               | ഡ  ɖ               |                   |ദ   d         |              |ബ  b     ||
|ഘോഷം (Plosive, V, A)| ഘ  gʰ            | ഝ   ɟʰ            | ഢ ɖʰ                |                   |ധ  dʰ      |              |ഭ  bʰ     ||
|അനുനാസികം (Nasal)   | ങ ŋ             | ഞ  ɲ             | ണ  ɳ               | ഩ n               |ന n̪           |             |മ m      ||
|മദ്ധ്യമം (Glide)|                   | യ  j             |                     |                   |               |വ  v |           ||
|മദ്ധ്യമം (Approximant)|           |                  |ഴ  ʐ                  |                |          |   |     ||
|മദ്ധ്യമം (Lateral Approximant)|           |                  |ള  ɭ                  |ല l                 |           |   |     ||
|മദ്ധ്യമം (Tap)|           |               |               | ര r                   |                |   |  ||
|മദ്ധ്യമം (Trill)|           |               |              |റ  ṛ                   |                |  |   ||
|ഊഷ്മാക്കള്‍ (Fricatives)|                 |ശ  ʃ               |ഷ  ʂ               | സ  s                  |       | ഫ f   |       |ഹ h|

**Table 3**: The consonant graphemes in Malayalam. IPA representation of each grapheme is indicated. It follows the [phonetic archive project](http://www.cmltemu.in/phonetic/#/) scheme. But there are disagreements to this scheme in literature (discussed below)


### Comparison of IPA representation of Malayalam consonants with other sources

-  ക, ഖ, ഗ, ഘ are unanimously classified as velar plosives and ങ as velar nasal by A. R. R. Varma(1916), K. P. Mohanan(1986), Asher and Kumari(1997), Reenu Punnose(2010), Savithry Namboodiripad(2017) and [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/). The IPA symbols are as described in the above table.
-  ട, ഠ, ഡ, ഢ are unanimously classified as retroflex plosives and ണ as retroflex nasal by   A. R. R. Varma(1916), K. P. Mohanan(1986), Asher and Kumari(1997), Reenu Punnose(2010), Savithry Namboodiripad(2017) and [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/). The IPA symbols are as described in the table.
- പ, ഫ, ബ, ഭ are unanimously classified as labial plosives and മ as labial nasal by A. R. R. Varma(1916), K. P. Mohanan(1986), Asher and Kumari(1997), Reenu Punnose(2010), Savithry Namboodiripad(2017) and [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/). The IPA symbols are as described in the table.
- ച, ഛ, ജ, ഝ 
    - These are described as palatal plosives  by A. R. R. Varma(1916), [phonetic archive project](http://www.cmltemu.in/phonetic/#/) and Reenu Punnose(2010). The above IPA in the table is used by [phonetic archive project](http://www.cmltemu.in/phonetic/#/) and Reenu Punnose(2010). 
    - But Asher and Kumari(1997) suggests that they are slightly affricated.
    - Savithry Namboodiripad(2017) observes these as lamino postalveolar affricates with IPAs /t̠͡ɕ/, /t̠͡ɕh/, /d̠͡ʑ/, and /d̠͡ʑɦ/
    - [IPA/Malayalam](https://en.wikipedia.org/wiki/Help:IPA/Malayalam) Help page on wikipedia follows a different IPA for ച, ഛ as /t͡ʃ/and /t͡ʃʰ/, indicating affricated nature. 
- ഞ is classified as palatal nasal by  A. R. R. Varma(1916), K. P. Mohanan(1986), Asher and Kumari(1997), Reenu Punnose(2010), Savithry Namboodiripad(2017) and [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/). The IPA symbols is as described in the table.
- ഺ is described as alveolar plosive by all sources. But the grapheme ഺ is not popular in regular usage or literature. Only A. R. R. Varma(1916) uses it.
    - The [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/) uses the grapheme റ്റ and the IPA [ṟ] to describe it
    - Asher and Kumari(1997), Reenu Punnose(2010), Savithry Namboodiripad(2017) and K. P. Mohanan(1986)  uses the IPA [t] to describe it.
- ഩ is described as alveolar nasal by all sources. But the grapheme ഩ is not popular in regular usage or literature. Only A. R. R. Varma(1916) uses it.
    - The [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/) uses the grapheme ന and the IPA [n] to describe it. 
    - Asher and Kumari(1997), Reenu Punnose(2010), Savithry Namboodiripad(2017) and K. P. Mohanan(1986)  uses the IPA [n] to describe it.
- ത, ഥ, ദ, ധ  are unanimously classified as dental plosives and ന as dental nasal by  [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/), Asher and Kumari(1997), Reenu Punnose(2010), Savithry Namboodiripad(2017) and K. P. Mohanan(1986). 
    - The IPA symbols are as described in the table for [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/)
    - All other sources (Asher and Kumari(1997), Savithry Namboodiripad(2017) and Reenu Punnose(2010)) uses a combining bridge below ( ̪) to convert the corresponding alveolar plosives and nasals to dental plosives and nasals as [t̪] [t̪ʰ] [d̪] [d̪ʱ] [n̪]
    - K. P. Mohanan(1986) uses an underline to indicate the dental sound instead of the combining bridge below.
- യ is the voiced palatal approximant (with glide  or semi-vowel properties) and has the symbol [j] in IPA. Its manner and position is unanimously accepted by A. R. R. Varma(1916), K. P. Mohanan(1986),Asher and Kumari(1997), Reenu Punnose(2010), Savithry Namboodiripad(2017)  and [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/).
- വ is the voiced labiodental approximant 
    - It has glide  or semi-vowel properties and has the symbol /v/ in IPA as per Asher and Kumari(1997) and [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/). Asher and Kumari(1997) describes two possible allophones /ʋ/ or /w/.
    - According to Savithry Namboodiripad(2017) it is labiodental approximant and IPA is /ʋ/.
    - Its position  of articulation is unanimously accepted by A. R. R. Varma(1916), K. P. Mohanan(1986), , Reenu Punnose(2010), Savithry Namboodiripad(2017) and Asher and Kumari(1997) 
- ശ is unambiguously fricative. But position of articulation and IPA representation differ in different literature sources
    - According to A. R. R. Varma(1916) it is palatal fricative
    - Palatal fricative as per [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/) with ʃ as the IPA representation.
    - Lamino palatoalveolar fricative as per Asher and Kumari(1997) with ʃ as the IPA representation
    - Alveolar fricative as per K. P. Mohanan(1986)
    - Alveolar fricative as per Reenu Punnose(2010) with ʃ as the IPA representation.
    - Savithry Namboodiripad(2017) observes ശ as voiceless lamino postalveolar fricative with IPA /ɕ/.
    - According to IPA the symbol for palatal fricative is /ç/,  post-alveolar fricative is /ʃ/ and voiceless alveolo-palatal fricative is /ɕ/
- ഷ is unambiguously fricative. But position of articulation and IPA representation differ in different literature sources.
    - Retroflex fricative according to A. R. R. Varma(1916).
    - Lamino-palatoalveolar ('retroflex') fricative as per Asher and Kumari(1997) with /ʂ/ as IPA symbol
    - Retroflex fricative as per  K. P. Mohanan(1986), Reenu Punnose(2010) and [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/) and Savithry Namboodiripad(2017) with /ʂ/ as IPA representation.
- സ is unambiguously fricative 
    - Position of articulation is dental according to A. R. R. Varma(1916)
    - Apico-alveolar as per Asher and Kumari(1997) and the IPA representation is /s/
    - Alveolar fricative as per K. P. Mohanan(1986), Reenu Punnose(2010)and [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/)with the IPA     representation as /s/
    - Dental fricative as per Savithry Namboodiripad(2017) with IPA /s̪/
- ഹ is glottal fricative as per A. R. R. Varma(1916), K. P. Mohanan(1986), Asher and Kumari(1997), Reenu Punnose(2010), Savithry Namboodiripad(2017) and [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/) with IPA /h/
- ഫ is the labial fricative. But the grapheme used is same as the labial aspirated plosive. There are no native Malayalam words with this sound. but it is used in loan words as in ഫാന്‍. It has an IPA /f/. This sound is not indicated in [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/) and A. R. R. Varma(1916). But all other sources under study describes it.Hence it is included in the table above.
- ല - The articulatory realization of this consonant is dental lateral according to A. R. R. Varma(1916)
    - The [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/), K. P. Mohanan(1986) and Savithry Namboodiripad(2017) describes it as alveolar lateral with IPA /l/.
    - Asher and Kumari(1997) describes it as apico-alveolar lateral with IPA /l/.
    - Reenu Punnose(2010) describes it as alveolar lateral. The tongue has laminal(convex) shape during articulation resulting in a clear resonance pattern with higher [second formant frequency](https://en.wikipedia.org/wiki/Formant).
- ള is another lateral according to all sources A. R. R. Varma(1916), K. P. Mohanan(1986), Asher and Kumari(1997), Reenu Punnose(2010) and [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/).
    - It is alveolar (and retroflex) as per A. R. R. Varma(1916) 
    - Retroflex as per [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/) with IPA /ɭ/.
    - It is sublamino-palatal ('retroflex') lateral as per  Asher and Kumari(1997) with IPA /ɭ/.
    - It is retroflex lateral according to Reenu Punnose(2010). According to her the tongue has subapical(concave) contact during articulation. It results in darker resonance with lower [second formant frequency](https://en.wikipedia.org/wiki/Formant). It is described with IPA  /ɭ/.
- ര is alveolar in its position of articulation and tap in its manner of articulation as per K. P. Mohanan(1986) and [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/). Its IPA is /r/ as per [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/), but with a line above as per K. P. Mohanan(1986)
    - As per A. R. R. Varma(1916) it is retroflex tap.
    - It is apico-dentialveolar tap with a distinct palatal resonance  according to Asher and Kumari(1997) with IPA /ɾ/
    - It is lamino- alveolar tap with palatalization as per Reenu Punnose(2010). It has a clear resonance pattern with higher [second formant frequency](https://en.wikipedia.org/wiki/Formant) and IPA /ɾ/
- റ is alveolar trill as per [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/) and has an IPA representation /ṛ/.
    - But it is retroflex trill as per A. R. R. Varma(1916)
    - Asher and Kumari(1997) describes it as  apico-alveolar tap or trill with IPA /r/.
    - Reenu Punnose(2010) describes it as apico-alveolar trill, realized during articulation as tap, trill or approximant with IPA /r/. It is slightly velarised with dark resonance properties, ie. lower [second formant frequency](https://en.wikipedia.org/wiki/Formant).
    - K. P. Mohanan(1986) describes it as alveolar tap with IPA /r/
- ഴ is retroflex approximant as per A. R. R. Varma(1916) and [Malayalam phonetic archive project](http://www.cmltemu.in/phonetic/#/) with IPA /ʐ/.
    - Asher and Kumari(1997) describes it as sublamino-palatal approximant with IPA /ʐ/.
    - Savithry Namboodiripad(2017) describes it as retroflex approximant with IPA /ɻ/
    - K. P. Mohanan(1986) describes it as retroflex frictionless continuant with IPA /ʐ/.
    - According to Reenu Punnose(2010), it is  Voiced post alveolar approximant, with no back-to-front tongue  movement like  retroflexes. But phonologically it shows the properties of retroflexes. She suggests a different symbol [ɻʲ] to indicate its phonological properties. i.e. a clear post-alveolar approximant (symbol for retroflex approximant with a plus diacritic underneath to suggest slightly fronter place of articulation than retroflex and a \j\ superscript to depict palatalised quality.

### Conclusion

1. This article is an attempt to describe the articulatory nature of Malayalam consonants. 

2. Many consonant graphemes have diverse manifestations depending on the speaker. That is the reason behind the differences in the phonetic properties recorded by different grammarians and phoneticians.

3. There has been many experiments to understand the true nature of consonants using auditory and articulatory studies. 

4. Auditory studies are based on the specrograms of speech while articulatory studies are done by ultrsound tongue traces. The [work](https://www.researchgate.net/publication/260152477_The_Contested_Fifth_Liquid_in_Malayalam_A_Window_into_the_Lateral-Rhotic_Relationship_in_Dravidian_Languages) of Reenu Punnoose(2013) is one such study. 

5. Since there is no unanimous scheme to to describe the Malayalam consonants, and there has been very little studies to reveal the true nature of Malayalam consonants, any work on grapheme to phoneme conversion requires a statement on the scheme followed to do the conversion. I will be working on this part and update my [Malayalam Phonetic Analyser](https://phon.smc.org.in/) soon.

Thanks!!



### References
1.  [The Anthropology of Language -  Harriet Joseph Ottenheimer , Judith M.S. Pine  ](https://www.amazon.com/Anthropology-Language-Introduction-Linguistic/dp/1337571008)1.  1. [കേരളപാണിനീയം](https://ml.wikisource.org/wiki/%E0%B4%95%E0%B5%87%E0%B4%B0%E0%B4%B3%E0%B4%AA%E0%B4%BE%E0%B4%A3%E0%B4%BF%E0%B4%A8%E0%B5%80%E0%B4%AF%E0%B4%82) - ഏ. ആര്‍. രാജരാജവര്‍മ്മ (Keralapanineeyam - A. R. Rajaraja Varma), 1816
2. [The Theory of Lexical Phonology](https://link.springer.com/book/10.1007/978-94-009-3719-2) - K. P. Mohanan, 1986
3. [Malayalam (Descriptive Grammars)](https://books.google.co.in/books/about/Malayalam.html?id=uFQG2DCaIsIC) - R. E. Asher, T. C. Kumari, 1997
4. [Malayalam Phonetic Archive](http://www.cmltemu.in/phonetic/#/)-  Thunchath Ezhuthachan Malayalam University
5. [Malayalam IPA Help](https://en.wikipedia.org/wiki/Help:IPA/Malayalam) page on English Wikipedia 
6. [An Auditory and Acoustic Study of Liquids in Malayalam](https://core.ac.uk/download/pdf/40022785.pdf) - Dr. Reenu Punnoose, 2010
7. [IPA Chart with sounds](https://www.internationalphoneticalphabet.org/ipa-sounds/ipa-chart-with-sounds/)
8.  [Malayalam (Namboodiri Dialect)](https://www.cambridge.org/core/journals/journal-of-the-international-phonetic-association/article/malayalam-namboodiri-dialect/19CBF6E9E1CE65A84928F7C9C2286A9B#fndtn-information) - Savithry Namboodiripad, Marc Garellek, 2017

