---
title: "Call me Dr. Kavya 🤩"
tags: ["phd" ]
date: 2024-03-05
description: "PhD Awarded"
---

I was awarded doctoral degree by APJ Abdul Kalam Technological University, Kerala, India.

You can read my thesis 'Linguistic challenges in Malayalam speech recognition: Analysis and solutions' [here](https://kavyamanohar.com/documents/Kavya-phd-2023.pdf).

Pics from the graduation ceremony hosted by College of Engineering Trivandrum and APJ Abdul Kalam Technological University.

{{< figure src="/img/phd/cet-convocation.JPG">}}


{{< figure src="/img/phd/convocation.png">}}
