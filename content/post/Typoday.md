---
title: "Typoday 2018"
tags: ["typography", "conference", "Manjari", "typeface", "font"]
date: 2018-03-03
---
{{< figure src="/img/2018-03-03-Typoday/typoday.jpg">}}

[Santhosh](https://thottingal.in/blog/about/) and I jointly presented a paper at [Typoday 2018](http://www.typoday.in/). The paper was titled 'Spiral splines in typeface design: A case study of Manjari Malayalam typeface'. The full paper is available [here](https://thottingal.in/documents/Spiral-Splines-Manjari.pdf). The presentation is available [here](https://thottingal.in/presentations/Typoday2018.pptx).


{{< figure src="/img/2018-03-03-Typoday/2.jpg">}}

Typoday is the annual conference where typographers and graphic designers from academia and industry come up with their ideas and showcase their work. Typoday 2018 was held at Convocation Hall, University of Mumbai.


{{< figure src="/img/2018-03-03-Typoday/1.jpg">}}

*Photo Credits: Santhosh Thottingal, Sunil Kudalkar, Abhishek Sahane*


<iframe width="560" height="315" src="https://www.youtube.com/embed/EFiE4xoJ9HI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
