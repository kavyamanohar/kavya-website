---
#title: What I do
comments: false
---
<!--
#### PROJECTS

1. Malayalam Speech to Text system using Kaldi and Vosk. 
    - [Demo](https://ccoreilly.github.io/vosk-browser/) 
    - [Repo](https://gitlab.com/kavyamanohar/vosk-malayalam)
2. FST based Malayalam [grapheme to phoneme converter](https://gitlab.com/smc/mlphon)
2. Opentype engineering and maintenance of Malayalam [fonts](https://smc.org.in/fonts/) by Swathanthra Malyalam Computing-->

#### PUBLICATIONS

##### Book Chapter

1. Sherly, Elizabeth, Leena G. Pillai, and Kavya Manohar. "[ASR Models from Conventional Statistical Models to Transformers and Transfer Learning.](https://doi.org/10.1002/9781394214624.ch6)" Automatic Speech Recognition and Translation for Low Resource Languages (2024): 69-112.

##### Journals

1. Manohar, K., A R, J. & Rajan, R. "[Improving speech recognition systems for the morphologically complex Malayalam language using subword tokens for language modeling](https://doi.org/10.1186/s13636-023-00313-7)". J AUDIO SPEECH MUSIC PROC. 2023, 47 (2023).

2. K. Manohar, A. R. Jayan and R. Rajan, "[Mlphon: A Multifunctional Grapheme-Phoneme Conversion Tool Using Finite State Transducers](https://doi.org/10.1109/ACCESS.2022.3204403)," in IEEE Access, vol. 10, pp. 97555-97575, 2022.

##### Conferences

1. Kavya Manohar, Leena G Pillai. 2024. "[What is lost in Normalization? Exploring Pitfalls in Multilingual ASR Model Evaluations](https://arxiv.org/abs/2409.02449)". In Proceedings of the 2024 Conference on Empirical Methods in Natural Language Processing, Miami, Florida. Association for Computational Linguistics.(Accepted)

1. Kavya Manohar, Ashish Abraham, Gokul G Menon (2024). "Enhancing End-to-End Malayalam Automatic
Speech Recognition with Language Model Augmentation". In: Chakravarthi, B.R., et al. Speech and Language Technologies for Low-Resource Languages. SPELLL 2024. (Accpeted)

1. B. Baiju, K. Manohar, L. G. Pillai and E. Sherly, "Malayalam to English Named Entity Transliteration using Attention based BiLSTM," 2024 IEEE Recent Advances in Intelligent Computational Systems (RAICS), Kothamangalam, Kerala, India, 2024, pp. 1-6, doi: [10.1109/RAICS61201.2024.10690040](10.1109/RAICS61201.2024.10690040).

1. K. Raju Basil, G. Pillai Leena, Manohar Kavya, and Sherly Elizabeth. 2023. [Automatic Speech Recognition System for Malasar Language using Multilingual Transfer Learning](https://aclanthology.org/2023.icon-1.41). In Proceedings of the 20th International Conference on Natural Language Processing (ICON), pages 472–477, Goa University, Goa, India. NLP Association of India (NLPAI).

1. K. Manohar, G. G. Menon, A. Abraham, R. Rajan and A. R. Jayan, "Automatic Recognition of Continuous Malayalam Speech using Pretrained Multilingual Transformers," 2023 International Conference on Intelligent Systems for Communication, IoT and Security (ICISCoIS), Coimbatore, India, 2023, pp. 671-675, doi: 10.1109/ICISCoIS56541.2023.10100598. [DOI](https://doi.org/10.1109/ICISCoIS56541.2023.10100598)

2. Manohar, Kavya, A. R. Jayan, and Rajeev Rajan. "[Syllable Subword Tokens for Open Vocabulary Speech Recognition in Malayalam.](https://aclanthology.org/2022.nsurl-1.1/)" In Proceedings of the Third International Workshop on NLP Solutions for Under Resourced Languages (NSURL 2022) co-located with ICNLSP 2022, pp. 1-7. 2022.

2. Kavya Manohar, A. R. Jayan, Rajeev Rajan (2020), [Quantitative Analysis of the Morphological Complexity of Malayalam Language](https://kavyamanohar.com/documents/tsd_morph_complexity_ml.pdf). In: Sojka P., Kopeček I., Pala K., Horák A. (eds) Text, Speech, and Dialogue. TSD 2020. Lecture Notes in Computer Science, vol 12284. Springer, Cham. [DOI](https://doi.org/10.1007/978-3-030-58323-1_7)

2. Kavya Manohar & Santhosh Thottingal (2019), Malayalam Orthographic Reforms. Impact on Language and Popular Culture, in Proceedings of Graphemics in the 21st Century, Brest 2018 (Yannis Haralambous, Ed.), Brest: Fluxus Editions, 329–351. [DOI](https://doi.org/10.36824/2018-graf-mano)

3. Kavya Manohar & Santhosh Thottingal (2018), [Spiral Splines in Typeface Design: A case study of Manjari Malayalam Typeface](https://www.typoday.in/2018/spk_papers/santhosh-thottingal-typoday-2018.pdf) in Typoday 2018, Mumbai.

3. K. Manohar and B. Premanand, "Comparative study on vector quantization codebook generation algorithms for wideband speech coding," 2012 International Conference on Green Technologies (ICGT), Trivandrum, 2012, pp. 082-088. [DOI](https://doi.org/10.1109/ICGT.2012.6477952)


#### TALKS

1. Invited talk on 'Malayalam in Unicode: Some Linguistic and Cultural Thoughts ([യൂണിക്കോഡിലെത്തിയ മലയാളം: ചില ഭാഷാസാംസ്കാരിക വിചാരങ്ങൾ](https://youtu.be/K_PGz2xu32c))' at Thapasam Seminar hosted by Sree Sankaracharya University of Sanskrit, Kaldy on October 2, 2024.

1. Invited talk on ‘Automatic Speech Recognition' at UGC Stride Faculty Development Programme and International Conference at WMO College, Wayanad on April 28, 2023.

1. Invited talk on 'Does your computer understand spoken Malayalam? ([കമ്പ്യൂട്ടർ മലയാളം കേൾക്കുമ്പോൾ](https://www.youtube.com/watch?v=aOMSRDaySpY))' as part of the Language Computing webinar series organized by Kerala Sasthra Sahithya Parishad on October 14, 2022.

2. Invited talk on '[Contributing to Free and Open Source Software](https://www.instagram.com/p/CSOtME_JaW0/?hl=en)' at the the *init.d* workshop organized by the FOSS cell, Model Engineering College Thrikkakkara, Ernakulam on 7th August 2021.  

3. Invited talk: 'Indic Scripts, Unicode and a Brief Introduction to Natural Language Processing' as a part of [National webinar series](http://ssvcollege.ac.in/wp-content/uploads/2020/10/NATIONAL-WEBINAR-SERIES-SECOND-SESSION-CS-DEPARTMENT-SSV-COLLEGE-VALAYANCHIRANGARA.pdf) organized by the Department of Computer Science at [Sree Sankara Vidyapeetom College](https://ssvcollege.ac.in/), Eranakulam on October 20, 2020.

6. Talk on 'The Digital Representation of Malayalam (കമ്പ്യൂട്ടറിലെ എഴുത്തും വായനയും)' at the Malayalam Computing Workshop organized by the Malayalam Department of the University of Kerala, on December 11, 2019.

5. [Grafematik](http://conferences.telecom-bretagne.eu/grafematik/), 2018, Brest, France. Presented a paper on the impacts of Malayalam orthographic reforms. [Talk Video](https://www.youtube.com/watch?v=-KvFuuVix4Q).

4.  Typoday 2018, Mumbai. Presented a paper with Santhosh Thottingal on the design principles of Manjari typeface titled: '[Spiral splines in Typeface Design: A case study of Manjari Malayalam typeface](http://typoday.in/spk_papers/santhosh-thottingal-typoday-2018.pdf)'. Talk Video is available [here](https://www.youtube.com/watch?v=EFiE4xoJ9HI).

3.  Swathanthra Malayalam Computing 12th Anniversary Celebrations, Thrissur, 2013 - Talk on Malayalam Font Development and related Technologies.

2.  Wikimania 2013, Hong Kong- [Talk](https://www.youtube.com/watch?v=jNat0IEi3Jg) delivered on a survey conducted among my students and colleagues on the usage pattern of online resources for learning.

1.  International Conference on Green Technologies, ICGT 2012, Thiruvananthapuram : Paper presented on my master's thesis on [Vector Quantization Codebook generation](http://ieeexplore.ieee.org/xpl/login.jsp?tp=&arnumber=6477952&url=http%3A%2F%2Fieeexplore.ieee.org%2Fxpls%2Fabs_all.jsp%3Farnumber%3D6477952).

#### WORKSHOPS
1. LaTeX tutorial and hands-on training workshop organized by the Malayalam department at the University of Kerala, with support from [ICFOSS](https://icfoss.in/) during Dec 22nd and 23rd, 2018. A sample template for thesis report in Malayalam prepared for the workshop is available [here](https://gitlab.com/kavyamanohar/thesis-template-mal).
3.  Workshop on Electronic Circuit Simulation and analysis tool- [eSim](http://esim.fossee.in/) at Aryanet Institute of Technology, Palakkad on 30th November, 2016.
2.  [CEC ISQIP’16](http://isqip.cecieee.org/)- IEEE Student Quality Improvement Programme at College of Engineering Chengannur. Led an introductory workshop on Version Control Systems.
1.  Introductory workshop and hands-on training:'Report making using LateX' at Aryanet Institute of Technology, Palakkad on 9th June, 2015.

#### Lab Manuals for Undergraduate Courses

1.  A Lab manual for basic [Analog Communication Lab](http://thottingal.in/blog/2014/04/13/analogcommunication-labmanual/).
<!-- 4.  A Lab manual for basic [Electronic Circuits Lab using eSim](http://thottingal.in/blog/2015/12/04/experimenting-esim-a-tool-for-electronic-circuit-simulation/). -->
2.  A Labmanual for [Communication Engineering (Analog and Digital) Lab](https://github.com/kavyamanohar/communicationengineering_lab)
<!-- 2.  The Scilab code and manual for [Digital Signal processing Lab](https://github.com/kavyamanohar/DSPlab) -->
