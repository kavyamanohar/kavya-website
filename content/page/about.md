---
title: About Kavya
//subtitle: Why you'd want to hang out with me
comments: false
description: "Personal Website of Kavya Manohar. Read about Malayalam Speech Recognition | Malayalam Speech to Text | Malayalam Voice Typing | Malayalam G2P | Grapheme to Phoneme Conversion "
---
{{< figure src="/img/kavya-website.jpg">}}


I work as a computational linguist in the [Virtual Resource Centre for Language Computing](https://www.iiitmk.ac.in/vrclc/team-members/) at [Kerala University of Digital Sciences Innovation and Technology](https://duk.ac.in). I love to share what I learn and hence this blog.

I was awarded PhD by [A. P. J. Abdul Kalam Technological University, Kerala](https://ktu.edu.in) for my [thesis](https://kavyamanohar.com/documents/Kavya-phd-2023.pdf) titled *Linguistic challenges in Malayalam speech recognition: Analysis and solutions* in October 2023. The work was carried out at [College of Engineering Trivandrum](https://www.cet.ac.in/) under the supervision of [Dr. A. R. Jayan](http://gecskp.ac.in/faculty/22/dr-a-r-jayan) and [Dr. Rajeev Rajan](http://www.rajeevrajan.in/).

I used to be a teacher in the field of electronics, signal processing and digital communication. I was employed as Assistant Professor at [Govt. Engineering college, Sreekrishnapuram, Palakkad](http://www.gecskp.ac.in/). 

I am involved in various projects on Malayalam language computing. An attempt to analyse Malayalam text phonetically using finite state transducers can be seen [here](https://gitlab.com/smc/mlphon).  I have had my hands wet in digital type design and open type engineering of a couple of fonts maintained by [Swathanthra Malayalam Computing](https://smc.org.in).

<!-- I maintain a project to create lab manuals for various lab courses for Electronics and Communication Engineering. The manuals created so far are published under CC-BY-SA licence. The repository of lab manuals is open for all stakeholders to collaborate and enrich its contents. These are hosted in [github](http://github.com/kavyamanohar). -->

I try to engage with the world immediate to me in the possible ways to make life better for everyone around. It was a moment of pride that upon a request filed by me to the Chief Minister of Kerala, the Director of Higher Education Kerala issued a [circular](https://blog.kavyamanohar.com/2014/05/blog-post_15.html) clarifying that there exists no regulations to insist the female teaching faculties to adhere to the dress code of saree.

In this blog I write about myself, my projects and whatever I feels like sharing with the world.
